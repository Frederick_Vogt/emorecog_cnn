import os
import cv2
import random
import time
import numpy as np

from collections import namedtuple
from sklearn.utils import shuffle

Image = namedtuple('Image', ['path', 'person_index', 'cls', 'filename', 'onehot_label'])


class DataHandler:

    def __init__(self, data_path, batch_size, classes, image_size, validation_fraction=0.2,
                 max_epochs=10, by_pid = False):
        """
        Constructor for the DataHandler.
        :param data_path: The path to the images.
        :param batch_size: Batch size of the returned batches.
        :param classes: The classes to load images for.
        :param max_epochs: Maximum number of epochs.
        """
        self.__curr_epoch = 0
        self.__curr_batch = 0
        self.__curr_valid_batch = 0
        self.__batch_size = batch_size
        self.__data_path = data_path
        self.__max_epochs = max_epochs
        self.__classes = classes
        self.__image_size = image_size
        self.LOG_FILE = 'log.txt'
        with open(self.LOG_FILE, 'w') as logfile:
            logfile.write('\n' + time.strftime('%a, %d %b %Y %H:%M:%S GMT',
                                               time.localtime()) + ":    Starting DataHandler")
        self.__train_data, self.__valid_data = self.__split_train_valid(self.__get_images_files_recursive(data_path, by_pid),
                                                                        validation_fraction, by_pid)

    def get_image_size(self):
        return self.__image_size

    def get_epochs_done(self):
        return self.__curr_epoch

    def get_batches_done(self):
        return self.__curr_batch

    def get_batches_per_epoch(self):
        return len(self.__train_data) / self.__batch_size

    def all_test_data_got(self):
        return self.__all_test_data_got

    @staticmethod
    def __split_train_valid(dataset, validation_fraction, by_pid):
        """
        Splits a dataset into train-, test and validation datasets.
        :param dataset: The dataset to split as dict of person_index => image list.
        :param validation_fraction: The fraction of validation data to split off.
        :param test_fraction: The fraction of test data to split off.
        :return: List of training data, list of validation data, list of test data
        """
        size = len(dataset)
        valid_amount = int(size * validation_fraction)

        validation_data = []
        train_data = []

        if by_pid:
            index_list = list(dataset)
            random.shuffle(index_list)
            for index in index_list[:valid_amount]:
                validation_data.extend(dataset[index])
            for index in index_list[valid_amount:]:
                train_data.extend(dataset[index])
        else:
            random.shuffle(dataset)
            validation_data = dataset[:valid_amount]
            train_data = dataset[valid_amount:]

        return train_data, validation_data

    def __load_and_normalize_image(self, file, image_size):
        """
        Load, resize and normalize an image.
        :param file: The full filename of the image.
        :param image_size: The size to resize the image to.
        :return: The resized and normalized image.
        """
        try:
            image = cv2.imread(file)
            image = cv2.resize(image, (image_size, image_size), 0, 0, cv2.INTER_LINEAR)
            image = image.astype(np.float32)
            image = np.multiply(image, 1.0 / 255.0)
        except:
            image = None
            print("Broken image file: " + str(file))
            with open(self.LOG_FILE, 'a') as logfile:
                logfile.write('\n' + time.strftime('%a, %d %b %Y %H:%M:%S GMT',
                                                   time.localtime()) + ":    Broken image file: " + str(file))
        return image

    def __get_images_files_recursive(self, dir, by_pid = False):
        """
        Recursively searches all files in the directory and all sub directories.
        :param dir: The root dir for the search
        :return: a list of all png and jpg files in the dir and sub dir that have a class in their filename.
        """
        if by_pid:
            images = {}
        else:
            images = []
        i = 0
        print('Searching files...')
        with open(self.LOG_FILE, 'a') as logfile:
            logfile.write(
                '\n' + time.strftime('%a, %d %b %Y %H:%M:%S GMT', time.localtime()) + ':    Searching dir: ' + dir)
        for dirpath, dirs, files in os.walk(dir):
            for filename in files:
                i += 1
                if i % 5000 == 0:
                    print(str(i) + ' images found.')
                if ('.jpg' in filename or '.png' in filename) and any(
                        class_string in filename for class_string in self.__classes):
                    cls = filename.split('_')[0]
                    # create one-hot encoded label
                    label = np.zeros(len(self.__classes))
                    label[self.__classes.index(cls)] = 1.0
                    person_index = filename.split('_')[2]
                    if person_index in images and by_pid:
                        images[person_index].append(
                            Image(path=os.path.join(dirpath, filename),
                                  person_index=person_index,
                                  cls=cls,
                                  filename=filename,
                                  onehot_label=label
                                  ))
                    elif by_pid:
                        images[person_index] = []
                    else:
                        images.append(Image(path=os.path.join(dirpath, filename),
                                            person_index=person_index,
                                            cls=cls,
                                            filename=filename,
                                            onehot_label=label
                                            ))
        print('\n' + str(i) + ' images found in total.')
        with open(self.LOG_FILE, 'a') as logfile:
            logfile.write('\n' + time.strftime('%a, %d %b %Y %H:%M:%S GMT', time.localtime()) + ':    ' + str(i)
                          + ' images found in total.')
        # random.shuffle(images)
        return images

    def next_validation_batch(self):
        """
        Returns the next batch of images.
        :return: loaded and normalized images list, classes list, onehot-labels list
        """
        images = []
        onehot_labels = []
        classes = []
        start_index = self.__curr_valid_batch * self.__batch_size
        # end_index = start_index + self.__batch_size
        # If the current batch would exceed the training data: start a new epoch after this batch.
        # if end_index > len(self.__valid_data):
        if start_index + self.__batch_size > len(self.__valid_data):
            start_index = start_index - len(self.__valid_data)  # start at negative index
            # end_index = start_index + self.__batch_size
            self.__curr_valid_batch = 0

        # for index in range(start_index, end_index):
        index = start_index
        while len(images) < self.__batch_size:
            image_named_tuple = self.__valid_data[index]
            image = self.__load_and_normalize_image(image_named_tuple.path, self.__image_size)
            if image is None:
                continue
            else:
                images.append(image)
                classes.append(image_named_tuple.cls)
                onehot_labels.append(image_named_tuple.onehot_label)
                index += 1
                if index > len(self.__valid_data):
                    index = 0
        self.__curr_valid_batch += 1
        return images, onehot_labels, classes

    def next_train_batch(self):
        """
        Returns the next batch of images.
        :return: loaded and normalized images list, classes list, onehot-labels list
        """
        while self.__curr_epoch < self.__max_epochs:
            images = []
            onehot_labels = []
            classes = []
            start_index = self.__curr_batch * self.__batch_size
            # end_index = start_index + self.__batch_size
            # If the current batch would exceed the training data: start a new epoch after this batch.
            if start_index + self.__batch_size > len(self.__train_data):
                start_index = start_index - len(self.__train_data)  # start at negative index
                # end_index = start_index + self.__batch_size
                self.__curr_epoch += 1
                self.__curr_batch = 0
                random.shuffle(images)
                with open(self.LOG_FILE, 'a') as logfile:
                    logfile.write('\n' + time.strftime('%a, %d %b %Y %H:%M:%S GMT',
                                                       time.localtime()) + ':    Starting epoch ' + str(
                        self.__curr_epoch))

            # for index in range(start_index, end_index):
            index = start_index
            while len(images) < self.__batch_size:
                image_named_tuple = self.__train_data[index]
                image = self.__load_and_normalize_image(image_named_tuple.path, self.__image_size)
                if image is None:
                    continue
                else:
                    images.append(image)
                    classes.append(image_named_tuple.cls)
                    onehot_labels.append(image_named_tuple.onehot_label)
                    index += 1
                    if index > len(self.__valid_data):
                        index = 0
            self.__curr_batch += 1
            yield images, onehot_labels, classes
