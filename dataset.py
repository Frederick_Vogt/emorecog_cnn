import cv2
import os
import glob
from sklearn.utils import shuffle
import numpy as np


def load_images(file_list, image_size):
    images = []
    for fl in file_list:
        image = cv2.imread(fl)
        image = cv2.resize(image, (image_size, image_size), 0, 0, cv2.INTER_LINEAR)
        image = image.astype(np.float32)
        image = np.multiply(image, 1.0 / 255.0)
        images.append(image)
    images = np.array(images)
    return images


def load_train(train_path, classes):
    labels = []
    img_names = []
    cls = []
    images_files = []
    print('Going to read training images')
    for fields in classes:
        index = classes.index(fields)
        print('Now going to read {} files (Index: {})'.format(fields, index))
        path = os.path.join(train_path, fields, '*g')
        files = glob.glob(path)
        for fl in files:
            images_files.append(fl)
            label = np.zeros(len(classes))
            label[index] = 1.0
            labels.append(label)
            flbase = os.path.basename(fl)
            img_names.append(flbase)
            cls.append(fields)
    labels = np.array(labels)
    img_names = np.array(img_names)
    cls = np.array(cls)

    return images_files, labels, img_names, cls


class DataSet:

    def __init__(self, images, labels, img_names, cls, image_size):
        self._num_examples = len(images)
        self._image_files = images
        self._labels = labels
        self._img_names = img_names
        self._cls = cls
        self._image_size = image_size
        self._epochs_done = 0
        self._index_in_epoch = 0

    @property
    def image_size(self):
        return self._image_size

    @property
    def image_files(self):
        return self._image_files

    @property
    def labels(self):
        return self._labels

    @property
    def img_names(self):
        return self._img_names

    @property
    def cls(self):
        return self._cls

    @property
    def num_examples(self):
        return self._num_examples

    @property
    def epochs_done(self):
        return self._epochs_done

    def next_batch(self, batch_size):
        """Return the next `batch_size` examples from this data set."""
        start = self._index_in_epoch
        self._index_in_epoch += batch_size

        if self._index_in_epoch > self._num_examples:
            # After each epoch we update this
            self._epochs_done += 1
            start = 0
            self._index_in_epoch = batch_size
            assert batch_size <= self._num_examples
        end = self._index_in_epoch

        return load_images(self._image_files[start:end], self._image_size), \
               self._labels[start:end], \
               self._img_names[start:end], \
               self._cls[start:end]


def read_train_sets(train_path, image_size, classes, validation_size):
    class DataSets(object):
        pass

    data_sets = DataSets()

    images_files, labels, img_names, cls = load_train(train_path, classes)
    images_files, labels, img_names, cls = shuffle(images_files, labels, img_names, cls)

    if isinstance(validation_size, float):
        validation_size = int(validation_size * len(images_files))

    validation_images = images_files[:validation_size]
    validation_labels = labels[:validation_size]
    validation_img_names = img_names[:validation_size]
    validation_cls = cls[:validation_size]

    train_images = images_files[validation_size:]
    train_labels = labels[validation_size:]
    train_img_names = img_names[validation_size:]
    train_cls = cls[validation_size:]

    data_sets.train = DataSet(train_images, train_labels, train_img_names, train_cls, image_size)
    data_sets.valid = DataSet(validation_images, validation_labels, validation_img_names, validation_cls, image_size)

    return data_sets
