import tensorflow as tf
import numpy as np
import os,glob,cv2
import sys,argparse


# Pass the path of the image
dir_path = os.path.dirname(os.path.realpath(__file__))
image_path=sys.argv[1] 
filename = image_path
image_size=160
num_channels=3
images = []
# Read the image using OpenCV
image = cv2.imread(filename)
# Resizing the image 
image = cv2.resize(image, (image_size, image_size),0,0, cv2.INTER_LINEAR)
images.append(image)
images = np.array(images, dtype=np.uint8)
images = images.astype('float32')
images = np.multiply(images, 1.0/255.0) 
x_batch = images.reshape(1, image_size,image_size,num_channels)

## Restore saved model 
sess = tf.Session()
saver = tf.train.import_meta_graph('dogs-cats-model.meta') # Name of the pre-retrained model
saver.restore(sess, tf.train.latest_checkpoint('./'))

# Get the resored graph
graph = tf.get_default_graph()

# Get prediction tensor
y_pred = graph.get_tensor_by_name("y_pred:0")

# Feed the images to the input placeholders
x= graph.get_tensor_by_name("x:0") 
y_true = graph.get_tensor_by_name("y_true:0") 
y_test_images = np.zeros((1, 8))


# Create the feed dict
feed_dict_testing = {x: x_batch, y_true: y_test_images}
# Predict the emotions and return the result
result=sess.run(y_pred, feed_dict=feed_dict_testing)
print(result)
