import tensorflow as tf
import numpy as np
import os
import glob
import cv2
import sys
import argparse

from random import shuffle

emotions = ['anger', 'contempt', 'disgust', 'fear', 'happiness', 'neutral', 'sadness', 'surprise']
error_images = []


def crop_face(image):
    """
    Crops an image to the first contained face.
    :param image: The image to crop.
    :return: The image cropped to the first contained image.
    """
    facedata = "haarcascade_frontalface_default.xml"
    cascade = cv2.CascadeClassifier(facedata)

    minisize = (image.shape[1], image.shape[0])
    miniframe = cv2.resize(image, minisize)

    faces = cascade.detectMultiScale(miniframe)

    for f in faces:
        x, y, w, h = [v for v in f]
        sub_face = image[y:y + h, x:x + w]
        # only return first face
        return sub_face
    return None


def load_get_paths_in_dir_recursive(dir):
    """
    Recursively searches all files in the directory and all sub directories.
    :param dir: The root dir for the search
    :return: a list of all png and jpg files in the dir and sub dir
    """
    file_list = list()
    i = 0
    print('Searching files')
    for dirpath, dirs, files in os.walk(dir):
        for filename in files:
            i += 1
            if '.jpg' in filename or '.png' in filename:
                file_list.append(os.path.join(dirpath, filename))
    print('\n' + str(len(file_list)) + ' images found.')
    return file_list


def classify_images(image_file_list, image_size, num_channels, model_path, checkpoint_dir, batch_size):
    """
    Classifies a list of image files.
    :param image_file_list: The list of image files to classify.
    :param image_size: The size to scale the images to.
    :param num_channels: The number of channels (has to be euqal to training).
    :param model_path: The path to the trained model.
    :param batch_size: The batch size to classify images in.
    :return: A list of predictions for the images.
    """
    print('Preprocessing images')
    global error_images
    images = []
    if FLAGS.save:
        if not os.path.isdir('cropped_images'):
            os.mkdir('cropped_images')
    for image_file in image_file_list:
        try:
            image = cv2.imread(image_file)
            if FLAGS.crop:
                image = crop_face(image)
            image = cv2.resize(image, (image_size, image_size), 0, 0, cv2.INTER_LINEAR)
            if FLAGS.save:
                new_filename, _ = os.path.splitext(image_file)
                new_filename = os.path.basename(new_filename)
                cv2.imwrite('cropped_images/' + new_filename + '.png', image)
            images.append(image)
        except cv2.error as error:
            print('Error processing ' + image_file + ': ' + str(error))
            error_images.append(image_file)
    # convert images to np array
    images = np.array(images, dtype=np.uint8)
    images = images.astype('float32')
    # normalize images
    images = np.multiply(images, 1.0 / 255.0)
    print('Starting tf')
    sess = tf.Session()
    # load trained net
    saver = tf.train.import_meta_graph(model_path)
    saver.restore(sess, tf.train.latest_checkpoint(checkpoint_dir))
    graph = tf.get_default_graph()
    # get layers for prediction
    y_pred = graph.get_tensor_by_name("y_pred:0")
    x = graph.get_tensor_by_name("input_placeholder:0")
    output_layer = graph.get_tensor_by_name("label_placeholder:0")
    results = []
    counter = 0
    total = int(len(image_file_list) / batch_size)
    for batch in [images[i:i + batch_size] for i in range(0, len(images), batch_size)]:
        print('Batch ' + str(counter) + ' of ' + str(total))
        counter += 1
        # result vector
        result_vector = np.zeros((1, 8))
        # create batch placeholder
        x_batch = batch.reshape(len(batch), image_size, image_size, num_channels)
        # create feed dict
        feed_dict_testing = {x: x_batch, output_layer: result_vector}
        # get predictions
        results.append(sess.run(y_pred, feed_dict=feed_dict_testing))
    return results


def save_classification_result(classification_results, image_file_list, output_filename):
    """
    Saves the result of image classification to a file.
    :param classification_results: The results to write to file.
    :param image_file_list: The classified image filename list.
    :param output_filename: The filename to output to.
    """
    global emotions
    correct = 0
    incorrect = 0
    total = 0
    with open(output_filename, 'w') as file:
        for sub_result in classification_results:
            for result_index in range(0, len(sub_result)):
                highest_index = np.argmax(sub_result[result_index])
                image_file_name = image_file_list[total]
                split_filename = image_file_name.split('/')
                image_file_name = split_filename[len(split_filename) - 1]
                emotion = emotions[highest_index]
                file.write('%-30s: %-10s' % (image_file_name, emotion))
                total += 1
                if emotion in image_file_name:
                    file.write('    CORRECT   (')
                    correct += 1
                else:
                    file.write('    INCORRECT (')
                    incorrect += 1
                for value in range(0, len(sub_result[result_index])):  # classification_results[result_index]:
                    curr_emotion = emotions[value]
                    if curr_emotion == emotion:
                        curr_emotion = curr_emotion.upper()
                    file.write(curr_emotion + ': %-7s ; ' % (str(round(sub_result[result_index][value], 4))))
                file.write(')\n')
        file.write(
            '\n\nCorrect: ' + str(correct) + ' (' + str(100 * round(correct / total, 2)) + '%)\nIncorrect: ' + str(
                incorrect) + ' (' + str(100 * round(incorrect / total, 2)) + '%)\nTotal: ' + str(total))


def main():
    """
    Main method for image classification.
    """
    # Get all images in the dir.
    image_list = load_get_paths_in_dir_recursive(FLAGS.image_path)
    # Classify the images.
    results = classify_images(image_list, FLAGS.image_size, FLAGS.channels, FLAGS.model_path, FLAGS.checkpoint_dir,
                              FLAGS.batch_size)
    # Get only images without error in preprocessing.
    images_without_error = [image for image in image_list if image not in error_images]
    # Save all classified images and their classification to file.
    save_classification_result(results, images_without_error, FLAGS.output_file)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'image_path',
        type=str,
        help='Absolute path to image .'
    )

    parser.add_argument(
        'output_file',
        type=str,
        help=r'File to output classification result to.'
    )

    parser.add_argument(
        'model_path',
        type=str,
        help=r'Model file to predict images with.'
    )

    parser.add_argument(
        '-r',
        '--crop',
        action='store_true',
        help='If set, images will be cropped to the face before classification.'
    )

    parser.add_argument(
        '-v',
        '--save',
        action='store_true',
        help='If true, cropped images are saved in a folder in the script folder.'
    )

    parser.add_argument(
        '-p',
        '--checkpoint_dir',
        type=str,
        default='./',
        help='Directory containing checkpoint files. Default: script dir (./)'
    )

    parser.add_argument(
        '-s',
        '--image_size',
        type=int,
        default=160,
        help=r'Size of the square images to predict. Default: 160'
    )

    parser.add_argument(
        '-c',
        '--channels',
        type=int,
        default=3,
        help=r'Number of channels per image. Defauls: 3'
    )

    parser.add_argument(
        '-b',
        '--batch_size',
        type=int,
        default=128,
        help=r'Batch size for batch wise classification. Default: 128'
    )

    FLAGS, unparsed = parser.parse_known_args()
    main()
