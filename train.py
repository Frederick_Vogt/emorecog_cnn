import argparse
import time
import math
import random
import time
import datetime
import os
import numpy as np
import tensorflow as tf

from data_handler import DataHandler

# Seeds the numpy RNG for reproducability.
from numpy.random import seed

seed(1)

# Seeds the tensorflow RNG for reproducability.
from tensorflow import set_random_seed

tf.set_random_seed(2)

# Classes
classes = ['anger', 'contempt', 'disgust', 'fear', 'happiness', 'neutral', 'sadness', 'surprise']
num_classes = len(classes)

# Network parameters
filter_size_conv1 = 3
num_filters_conv1 = 32

filter_size_conv2 = 3
num_filters_conv2 = 32

filter_size_conv3 = 16
num_filters_conv3 = 64

filter_size_conv4 = 16
num_filters_conv4 = 64

filter_size_conv5 = 64
num_filters_conv5 = 64

filter_size_conv6 = 64
num_filters_conv6 = 64

fc_layer_size = 128


def create_weights(shape):
    """
    Creates weights of given shape.
    :param shape: The shape of the biases to create.
    :return: The created weights.
    """
    return tf.Variable(tf.truncated_normal(shape, stddev=0.05))


def create_biases(size):
    """
    Creates biases of given size.
    :param size:  The size of the biases to create (count).
    :return: The biases.
    """
    return tf.Variable(tf.constant(0.05, shape=[size]))


def create_convolutional_layer(input, num_input_channels, conv_filter_size, num_filters):
    """
    Creates a convolutional layer for the CNN.
    :param input: The input.
    :param num_input_channels: The number of channels of the data.
    :param conv_filter_size: The filter size for the layer.
    :param num_filters: The number of filters for the layer.
    :return: The convolutional layer.
    """
    weights = create_weights(shape=[conv_filter_size, conv_filter_size, num_input_channels, num_filters])
    biases = create_biases(num_filters)
    layer = tf.nn.conv2d(input=input,
                         filter=weights,
                         strides=[1, 1, 1, 1],
                         padding='SAME')
    layer += biases
    layer = tf.nn.max_pool(value=layer,
                           ksize=[1, 2, 2, 1],
                           strides=[1, 2, 2, 1],
                           padding='SAME')
    layer = tf.nn.elu(layer)
    return layer


def create_flatten_layer(layer):
    """
    Creates a flatten layer for the CNN.
    :param layer: The layer to create the flatten layer from.
    :return: The flatten layer.
    """
    layer_shape = layer.get_shape()
    num_features = layer_shape[1:4].num_elements()
    layer = tf.reshape(layer, [-1, num_features])
    return layer


def create_fc_layer(input, num_inputs, num_outputs, use_elu=True):
    """
    Creates a fully connected layer for the CNN-
    :param input: The input.
    :param num_inputs: The number of input neurons.
    :param num_outputs: The number of output neurons.
    :param use_elu: If true the elu activation function is used for the layer.
    :return: The fully connected layer.
    """
    weights = create_weights(shape=[num_inputs, num_outputs])
    biases = create_biases(num_outputs)

    layer = tf.matmul(input, weights) + biases
    if use_elu:
        layer = tf.nn.elu(layer)

    return layer


def create_and_train_cnn(training_data, training_log_filename, train_start_time):
    """
    Creates and trains the CNN.
    :param training_data: The data to train on as a DataHandler object.
    :param training_log_filename: The filename to write the training log to.
    :param train_start_time: The global training start time for the filenames.
    """

    session = tf.Session()

    input_placeholder = tf.placeholder(tf.float32, shape=[None, FLAGS.img_size, FLAGS.img_size, FLAGS.channels],
                                       name='input_placeholder')

    # Label placeholder
    label_placeholder = tf.placeholder(tf.float32, shape=[None, num_classes], name='label_placeholder')
    y_true_cls = tf.argmax(label_placeholder, dimension=1)

    # Convolutional layers
    layer_conv1 = create_convolutional_layer(input=input_placeholder,
                                             num_input_channels=FLAGS.channels,
                                             conv_filter_size=filter_size_conv1,
                                             num_filters=num_filters_conv1)

    layer_conv2 = create_convolutional_layer(input=layer_conv1,
                                             num_input_channels=num_filters_conv1,
                                             conv_filter_size=filter_size_conv2,
                                             num_filters=num_filters_conv2)

    layer_conv3 = create_convolutional_layer(input=layer_conv2,
                                             num_input_channels=num_filters_conv2,
                                             conv_filter_size=filter_size_conv3,
                                             num_filters=num_filters_conv3)

    layer_conv4 = create_convolutional_layer(input=layer_conv3,
                                             num_input_channels=num_filters_conv3,
                                             conv_filter_size=filter_size_conv4,
                                             num_filters=num_filters_conv4)

    layer_conv5 = create_convolutional_layer(input=layer_conv4,
                                             num_input_channels=num_filters_conv4,
                                             conv_filter_size=filter_size_conv5,
                                             num_filters=num_filters_conv5)

    layer_conv6 = create_convolutional_layer(input=layer_conv5,
                                             num_input_channels=num_filters_conv5,
                                             conv_filter_size=filter_size_conv6,
                                             num_filters=num_filters_conv6)

    # Flatten layer
    layer_flat = create_flatten_layer(layer_conv6)

    # Fully connected layers
    layer_fc1 = create_fc_layer(input=layer_flat,
                                num_inputs=layer_flat.get_shape()[1:4].num_elements(),
                                num_outputs=fc_layer_size,
                                use_elu=True)

    layer_fc2 = create_fc_layer(input=layer_fc1,
                                num_inputs=fc_layer_size,
                                num_outputs=num_classes,
                                use_elu=False)

    y_pred = tf.nn.softmax(layer_fc2, name='y_pred')
    y_pred_cls = tf.argmax(y_pred, dimension=1)

    session.run(tf.global_variables_initializer())
    cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits=layer_fc2, labels=label_placeholder)
    cost = tf.reduce_mean(cross_entropy)
    optimizer = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(cost)
    correct_prediction = tf.equal(y_pred_cls, y_true_cls)
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    session.run(tf.global_variables_initializer())

    saver = tf.train.Saver()

    prev_epoch_nr = 0
    start_time = time.time()
    for x_batch, y_true_batch, cls_batch in training_data.next_train_batch():
        curr_epoch_nr = training_data.get_epochs_done()
        if training_data.get_batches_done() % 500 == 0:
            time_left = (training_data.get_batches_per_epoch() - training_data.get_batches_done()) * (
                    (time.time() - start_time) / training_data.get_batches_done())
            print('Training batch ' + str(training_data.get_batches_done()) + ' of ~' +
                  str(int(training_data.get_batches_per_epoch())) + ' in epoch ' + str(curr_epoch_nr) +
                  '. Time remaining for epoch: ' + str(int(time_left / 60)) + ' minutes')

        x_valid_batch, y_valid_batch, valid_cls_batch = training_data.next_validation_batch()
        feed_dict_tr = {input_placeholder: x_batch, label_placeholder: y_true_batch}
        feed_dict_val = {input_placeholder: x_valid_batch, label_placeholder: y_valid_batch}
        session.run(optimizer, feed_dict=feed_dict_tr)

        with open(training_log_filename, 'a') as train_log_file:
            train_log_file.write(time.strftime("%Y-%m-%d_%H:%M:%S", time.localtime()) + ':    ' + show_progress(session,
                                                                                                                curr_epoch_nr,
                                                                                                                training_data.get_batches_done(),
                                                                                                                feed_dict_tr,
                                                                                                                feed_dict_val,
                                                                                                                session.run(
                                                                                                                    cost,
                                                                                                                    feed_dict=feed_dict_val),
                                                                                                                accuracy) + '\n')

        if prev_epoch_nr != curr_epoch_nr:
            start_time = time.time()
            prev_epoch_nr = curr_epoch_nr
            val_loss = session.run(cost, feed_dict=feed_dict_val)
            print(show_progress(session, curr_epoch_nr, training_data.get_batches_done(), feed_dict_tr, feed_dict_val,
                                val_loss, accuracy, False))

            checkpoint_folder = os.path.join(os.path.join('checkpoints',
                                                          str(FLAGS.batch_size) + 'bs_' + str(
                                                              FLAGS.img_size) + 'px_' + str(
                                                              FLAGS.max_epochs) + 'e_' + str(train_start_time)),
                                             'epoch_' + str(curr_epoch_nr))
            if not os.path.isdir(checkpoint_folder):
                os.makedirs(checkpoint_folder)
            saver.save(session, os.path.join(checkpoint_folder, 'emotion_recognition_model_epoch_' + str(
                training_data.get_epochs_done()) + '_batch_' + str(training_data.get_batches_done())))


def show_progress(session, epoch, batch, feed_dict_train, feed_dict_validate, val_loss, accuracy, simple=True):
    """
    Puts the training progress in a formatted string.
    :param session: The tensorflow session.
    :param batch: The current batch.
    :param epoch: The current epoch.
    :param feed_dict_train: The training data.
    :param feed_dict_validate: The validation data.
    :param val_loss: The validation loss.
    :param accuracy: The accuracy.
    :param simple: If trie, a shortened string will be generated.
    :return: A formatted string containing loss and accuracies.
    """
    train_acc = session.run(accuracy, feed_dict=feed_dict_train)
    val_acc = session.run(accuracy, feed_dict=feed_dict_validate)
    if simple:
        return 'Epoch %2d  Batch %5d  T_Acc %5.2f%%  V_Acc %5.2f%%  V_Loss %.3f' % (
            epoch, batch, train_acc * 100, val_acc * 100, val_loss)
    else:
        return 'Epoch %2d Batch %5d: Training Accuracy: %4.2f%%, Validation Accuracy: %4.2f%%,  Validation Loss: %.3f' % (
            epoch, batch, train_acc * 100, val_acc * 100, val_loss)


def main():
    """
    Main method running the training.
    """
    start_time = time.strftime("%Y-%m-%d_%H:%M:%S", time.localtime())
    training_log_filename = str(FLAGS.batch_size) + 'bs_' + str(FLAGS.img_size) + 'px_' + str(
        FLAGS.max_epochs) + 'e_training_log.txt'
    with open(training_log_filename, 'w') as log_file:
        log_file.write('\n' + str(start_time) + ':    Starting training.\n')
    training_data = DataHandler(FLAGS.train_path, FLAGS.batch_size, classes, FLAGS.img_size, FLAGS.validation_size,
                                FLAGS.max_epochs, FLAGS.by_pid)
    create_and_train_cnn(training_data, training_log_filename, start_time)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'train_path',
        type=str,
        help='Path to training data.'
    )

    parser.add_argument(
        '-e',
        '--max_epochs',
        type=int,
        default=10,
        help=r'Number of training epochs. Default: 10'
    )

    parser.add_argument(
        '-v',
        '--validation_size',
        type=float,
        default=0.2,
        help=r'Fraction of validation images from the training set. Default: 0.2 = 20%'
    )

    parser.add_argument(
        '-p',
        '--by_pid',
        action='store_true',
        help='If set, training and validation data will be sorted by the person ID to prevent images of one person from occuring in training AND validation data.'
    )

    parser.add_argument(
        '-c',
        '--channels',
        type=int,
        default=3,
        help=r'Number of channels per image. Default: 3'
    )

    parser.add_argument(
        '-b',
        '--batch_size',
        type=int,
        default=64,
        help=r'Batch size for training and validation batches. Default: 64'
    )

    parser.add_argument(
        '-i',
        '--img_size',
        type=int,
        default=160,
        help=r'Image size for training data. Default: 160 (px squared)'
    )

    FLAGS, unparsed = parser.parse_known_args()
    main()
